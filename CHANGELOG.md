## 1.1.0 - 2019-01-15
* import from Github
* upgrade to Alpine 3.8.2
* add gitlab-ci

## 1.0.1 - 2017-12-05
* upgrade to Alpine 3.7 stable

## 1.0.0 - 2017-10-30
* first release
